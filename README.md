# Survey

## 1. Run locally
### 1.1. update .env files (first run only)
```
$ cd ${app_dir_path}
$ cp .env .env.local
$ cp ./docker/.env.template ./docker/.env.loc
```
### 1.2. update .env -> OAUTH_FACEBOOK_ID and OAUTH_FACEBOOK_SECRET (first run only)
### 1.3. Build and up application:
```
$ docker-compose --env-file ./docker/.env.loc -f ./docker/docker-compose.loc.yml up -d --build --force-recreate --remove-orphans
```
### 1.4. Update db-structure (first run only)
```
$ docker-compose -f ./docker/docker-compose.loc.yml exec quiz_php bin/console doctrine:migration:migrate
```
### 1.5. Build dependencies:
```
$ docker-compose -f ./docker/docker-compose.loc.yml exec quiz_php composer i
$ docker-compose -f ./docker/docker-compose.loc.yml exec quiz_php npm i
$ docker-compose -f ./docker/docker-compose.loc.yml exec quiz_php npm run build
```
### 1.6. Populate local /etc/hosts with:
```
${docker.ip} quiz.loc
```
### 1.7. App available on https://quiz.loc/
### 1.8. Add "ROLE_ADMIN" into database.user.roles for use admin-feature

## 2. Tests
### 2.1. checkout env:
```
APP_ENV=test
```
### 2.2. Prepare env:
```
$ php bin/console doctrine:schema:create
$ php bin/console doctrine:schema:update --force
$ php bin/console doctrine:fixtures:load
```
### 2.3. Run phpunit
```
$ ./bin/phpunit
```