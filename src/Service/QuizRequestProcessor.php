<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Quiz;
use App\Entity\QuizAnswer;
use App\Entity\User;
use App\Form\Type\QuizFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class QuizRequestProcessor
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var QuestionPaginator
     */
    private $questionPaginator;

    /**
     * @var QuizSessionManager
     */
    private $quizSessionManager;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var
     */
    private $form;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $em,
        QuestionPaginator $questionPaginator,
        QuizSessionManager $quizSessionManager,
        FormFactoryInterface $formFactory
    ) {
        $user = $tokenStorage->getToken()->getUser();
        if (!$user instanceof User) {
            throw new LogicException('User must be defined!');
        }

        $this->user = $user;
        $this->em = $em;
        $this->questionPaginator = $questionPaginator;
        $this->quizSessionManager = $quizSessionManager;
        $this->formFactory = $formFactory;
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function process(Request $request): bool
    {
        $currentPageNum = isset($request->get('quizzes')['page'])
            ? (int) $request->get('quizzes')['page']
            : 1;

        $quiz = $this->getPageQuiz($currentPageNum);

        $this->form = $this->formFactory->create(QuizFormType::class, $quiz, [
            'page' => $currentPageNum,
        ]);

        $this->form->handleRequest($request);

        if (!$this->form->isSubmitted() || !$this->form->isValid()) {
            return false;
        }

        $this->quizSessionManager->update($quiz);

        if (!$this->questionPaginator->isLastPage($currentPageNum)) {
            $quiz = $this->getPageQuiz(++$currentPageNum);
            $this->form = $this->formFactory->create(QuizFormType::class, $quiz, [
                'page' => $currentPageNum,
            ]);

            return false;
        }

        $quiz = $this->quizSessionManager->get();
        $this->quizSessionManager->clear();

        $this->em->persist($quiz);
        $this->em->flush();

        return true;
    }

    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param int $pageNum
     *
     * @return Quiz
     */
    private function getPageQuiz(int $pageNum): Quiz
    {
        $quiz = new Quiz($this->user);

        $questions = $this->questionPaginator->getPageQuestions($pageNum);

        foreach ($questions as $question) {
            $quizAnswer = new QuizAnswer();
            $quizAnswer->setQuestion($question);
            $quiz->addQuizAnswer($quizAnswer);
        }

        return $quiz;
    }
}