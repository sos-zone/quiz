<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\Quiz;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class QuizSessionManager
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $em
    ) {
        $this->session = $session;
        $this->em = $em;
    }

    public function update(Quiz $quiz)
    {
        if (!$this->session->has('quiz')) {
            $this->session->set('quiz', $quiz);
        } else {
            /** @var Quiz $sessionQuiz */
            $sessionQuiz = $this->session->get('quiz');

            foreach ($quiz->getQuizAnswers() as $quizAnswer) {
                $sessionQuiz->addQuizAnswer($quizAnswer);
            }

            $this->session->set('quiz', $sessionQuiz);
        }
    }

    /**
     * @return Quiz|null
     */
    public function get(): ?Quiz
    {
        if (!$this->session->has('quiz')) {
            return null;
        }

        $quiz = $this->session->get('quiz');

        // re-persist Entities from the session
        $quiz->setUser($this->em->getRepository(User::class)->find($quiz->getUser()->getId()));

        $questionRepository = $this->em->getRepository(Question::class);
        $answerRepository = $this->em->getRepository(Answer::class);
        foreach ($quiz->getQuizAnswers() as $quizAnswer) {
            /** QuizAnswer $quizAnswer */
            $quizAnswer->setQuestion($questionRepository->find($quizAnswer->getQuestion()->getId()));
            $quizAnswer->setAnswer($answerRepository->find($quizAnswer->getAnswer()->getId()));
        }

        return $quiz;
    }

    public function clear(): void
    {
        if ($this->session->has('quiz')) {
            $this->session->remove('quiz');
        }
    }
}