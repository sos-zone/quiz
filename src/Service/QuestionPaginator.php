<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Question;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

class QuestionPaginator
{
    private const QUESTIONS_PER_PAGE = 5;

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * @var Collection
     */
    private $questions;

    public function __construct(
        PaginatorInterface $paginator,
        EntityManagerInterface $em
    ) {
        $this->paginator = $paginator;
        $this->questions = $em->getRepository(Question::class)->findQuizQuestions();
    }

    /**
     * @param int $pageNum
     *
     * @return iterable
     */
    public function getPageQuestions(int $pageNum): iterable
    {
        return $this->paginator->paginate(
            $this->questions,
            $pageNum,
            self::QUESTIONS_PER_PAGE
        )->getItems();
    }

    /**
     * @param int $pageNum
     *
     * @return bool
     */
    public function isLastPage(int $pageNum): bool
    {
        $lastPage = (int) \ceil(\count($this->questions) / self::QUESTIONS_PER_PAGE);

        if ($pageNum > $lastPage) {
            throw new InvalidArgumentException('Invalid Argument in isLastPage() method');
        }

        return $pageNum === $lastPage;
    }
}