<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\QuizAnswer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuizAnswerFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();

            $quizAnswer = $form->getViewData();

            $form
                ->add('answer', ChoiceType::class, [
                    'choices' => $quizAnswer->getQuestion()->getAnswers()->getValues(),
                    'choice_label' => 'content',
                    'label' => $quizAnswer->getQuestion()->getContent(),
                    'label_attr' => [
                        'class' => 'font-weight-bold',
                    ],
                    'attr' => [
                        'class' => 'answers-panel',
                    ],
                    'expanded' => true,
                    'multiple' => false,
                ]);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => QuizAnswer::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'quizAnswers';
    }
}