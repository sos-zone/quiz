<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200609075756 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE quiz_answer (id INT AUTO_INCREMENT NOT NULL, quiz_id INT NOT NULL, question_id INT NOT NULL, answer_id INT NOT NULL, INDEX IDX_3799BA7C853CD175 (quiz_id), INDEX IDX_3799BA7C1E27F6BF (question_id), INDEX IDX_3799BA7CAA334807 (answer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quiz (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_A412FA92A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE question (id INT AUTO_INCREMENT NOT NULL, content LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE answer (id INT AUTO_INCREMENT NOT NULL, question_id INT NOT NULL, content LONGTEXT NOT NULL, weight INT NOT NULL, INDEX IDX_DADD4A251E27F6BF (question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE quiz_answer ADD CONSTRAINT FK_3799BA7C853CD175 FOREIGN KEY (quiz_id) REFERENCES quiz (id)');
        $this->addSql('ALTER TABLE quiz_answer ADD CONSTRAINT FK_3799BA7C1E27F6BF FOREIGN KEY (question_id) REFERENCES question (id)');
        $this->addSql('ALTER TABLE quiz_answer ADD CONSTRAINT FK_3799BA7CAA334807 FOREIGN KEY (answer_id) REFERENCES answer (id)');
        $this->addSql('ALTER TABLE quiz ADD CONSTRAINT FK_A412FA92A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE answer ADD CONSTRAINT FK_DADD4A251E27F6BF FOREIGN KEY (question_id) REFERENCES question (id)');

        $this->addSql('INSERT INTO question (id, content) VALUES (1, "My goals in life are clear")');
        $this->addSql('INSERT INTO answer (question_id, content, weight) VALUES 
                              (1, "No Answer", 0),
                              (1, "Strongly Disagree", 1),
                              (1, "Disagree", 2),
                              (1, "Neurtal", 3),
                              (1, "Agree", 4),
                              (1, "Strongly Agree", 5)
        ');

        $this->addSql('INSERT INTO question (id, content) VALUES (2, "If people are rude to me I just shrug it off")');
        $this->addSql('INSERT INTO answer (question_id, content, weight) VALUES 
                              (2, "No Answer", 0),
                              (2, "Strongly Disagree", 1),
                              (2, "Disagree", 2),
                              (2, "Neurtal", 3),
                              (2, "Agree", 4),
                              (2, "Strongly Agree", 5)
        ');

        $this->addSql('INSERT INTO question (id, content) VALUES (3, "I am confident in what I do")');
        $this->addSql('INSERT INTO answer (question_id, content, weight) VALUES 
                              (3, "No Answer", 0),
                              (3, "Strongly Disagree", 1),
                              (3, "Disagree", 2),
                              (3, "Neurtal", 3),
                              (3, "Agree", 4),
                              (3, "Strongly Agree", 5)
        ');

        $this->addSql('INSERT INTO question (id, content) VALUES (4, "I can work even when things are disorganised")');
        $this->addSql('INSERT INTO answer (question_id, content, weight) VALUES 
                              (4, "No Answer", 0),
                              (4, "Strongly Disagree", 1),
                              (4, "Disagree", 2),
                              (4, "Neurtal", 3),
                              (4, "Agree", 4),
                              (4, "Strongly Agree", 5)
        ');

        $this->addSql('INSERT INTO question (id, content) VALUES (5, "I consistently put full time and effort into everything I do")');
        $this->addSql('INSERT INTO answer (question_id, content, weight) VALUES 
                              (5, "No Answer", 0),
                              (5, "Strongly Disagree", 1),
                              (5, "Disagree", 2),
                              (5, "Neurtal", 3),
                              (5, "Agree", 4),
                              (5, "Strongly Agree", 5)
        ');

        $this->addSql('INSERT INTO question (id, content) VALUES (6, "I prefer achieving my goals than assisting others to achieve their goals")');
        $this->addSql('INSERT INTO answer (question_id, content, weight) VALUES 
                              (6, "No Answer", 0),
                              (6, "Strongly Disagree", 1),
                              (6, "Disagree", 2),
                              (6, "Neurtal", 3),
                              (6, "Agree", 4),
                              (6, "Strongly Agree", 5)
        ');

        $this->addSql('INSERT INTO question (id, content) VALUES (7, "I would describe myself as an extremely competent person")');
        $this->addSql('INSERT INTO answer (question_id, content, weight) VALUES 
                              (7, "No Answer", 0),
                              (7, "Strongly Disagree", 1),
                              (7, "Disagree", 2),
                              (7, "Neurtal", 3),
                              (7, "Agree", 4),
                              (7, "Strongly Agree", 5)
        ');

        $this->addSql('INSERT INTO question (id, content) VALUES (8, "I prefer working within a stable rather than flexible environment")');
        $this->addSql('INSERT INTO answer (question_id, content, weight) VALUES 
                              (8, "No Answer", 0),
                              (8, "Strongly Disagree", 1),
                              (8, "Disagree", 2),
                              (8, "Neurtal", 3),
                              (8, "Agree", 4),
                              (8, "Strongly Agree", 5)
        ');

        $this->addSql('INSERT INTO question (id, content) VALUES (9, "I am ambitious")');
        $this->addSql('INSERT INTO answer (question_id, content, weight) VALUES 
                              (9, "No Answer", 0),
                              (9, "Strongly Disagree", 1),
                              (9, "Disagree", 2),
                              (9, "Neurtal", 3),
                              (9, "Agree", 4),
                              (9, "Strongly Agree", 5)
        ');

        $this->addSql('INSERT INTO question (id, content) VALUES (10, "It\'s better to get a job done than aim for perfection")');
        $this->addSql('INSERT INTO answer (question_id, content, weight) VALUES 
                              (10, "No Answer", 0),
                              (10, "Strongly Disagree", 1),
                              (10, "Disagree", 2),
                              (10, "Neurtal", 3),
                              (10, "Agree", 4),
                              (10, "Strongly Agree", 5)
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quiz_answer DROP FOREIGN KEY FK_3799BA7C853CD175');
        $this->addSql('ALTER TABLE quiz_answer DROP FOREIGN KEY FK_3799BA7C1E27F6BF');
        $this->addSql('ALTER TABLE answer DROP FOREIGN KEY FK_DADD4A251E27F6BF');
        $this->addSql('ALTER TABLE quiz_answer DROP FOREIGN KEY FK_3799BA7CAA334807');
        $this->addSql('DROP TABLE quiz_answer');
        $this->addSql('DROP TABLE quiz');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE answer');
    }
}
