<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public const USER_EMAIL = 'user_email@email.coms';
    public const ADMIN_EMAIL = 'admin_email@email.coms';

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail(self::USER_EMAIL);
        $user->setRoles([User::ROLE_USER]);
        $user->setFacebookId($this->generateNDigitRandomNumber(15));
        $manager->persist($user);

        $admin = new User();
        $admin->setEmail(self::ADMIN_EMAIL);
        $admin->setRoles([User::ROLE_ADMIN]);
        $admin->setFacebookId($this->generateNDigitRandomNumber(15));
        $manager->persist($admin);

        $manager->flush();
    }

    private function generateNDigitRandomNumber($length){
        return \mt_rand(\pow(10,($length-1)), \pow(10,$length)-1);
    }
}
