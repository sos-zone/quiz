<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Question;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class QuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Question::class);
    }

    public function findQuizQuestions($maxResults = 10)
    {
        return $this->createQueryBuilder('q')
            ->orderBy('q.id', 'ASC')
            ->setMaxResults($maxResults)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findQuestionStatistics()
    {
        $statistics = $this->createQueryBuilder('q')
            ->select('q.id as question_id,
                            q.content as question_content,
                            a.id as answer_id,
                            a.content as answer_content,
                            COUNT(qa.id) as question_answers_count
            ')
            ->leftJoin('q.answers', 'a')
            ->leftJoin('a.quizAnswers', 'qa')
            ->orderBy('q.id', 'ASC')
            ->orderBy('a.id', 'ASC')
            ->groupBy('q.id')
            ->addGroupBy('a.id')
            ->getQuery()
            ->getArrayResult()
        ;

        // group by questions:
        $questions = [];
        foreach ($statistics as $item) {
            $questions[$item['question_id']][] = $item;
        }

        return $questions;
    }
}
