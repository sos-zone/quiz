<?php

declare(strict_types=1);

namespace App\Controller;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class FacebookController extends AbstractController
{
    /**
     * Action to start the "connect" process
     * @param ClientRegistry $clientRegistry
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function connect(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('facebook')
            ->redirect([
                // the scopes you want to access
                'public_profile',
                'email'
            ]);
    }

    /**
     * After going to Facebook, you're redirected back here
     * (configured in knpu_oauth2_client.yaml)
     *
     * @param Request $request
     * @param ClientRegistry $clientRegistry
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function connectCheck(
        Request $request,
        ClientRegistry $clientRegistry
    ) {
        return $this->redirectToRoute('home_page');
    }
}