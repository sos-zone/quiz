<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends AbstractController
{
    /**
     * @return Response
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function index()
    {
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $usersWithoutQuiz = $userRepository->findAllWithoutQuiz();

        return $this->render('admin/users.html.twig', [
            'usersWithoutQuiz' => $usersWithoutQuiz,
        ]);
    }
}