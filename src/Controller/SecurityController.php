<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends AbstractController
{
    /**
     * @return Response
    */
    public function login(): Response
    {
        return $this->render('security/login.html.twig');
    }

    /**
     * Controller can be blank: it will never be executed!
     *
     * @throws \Exception
     */
    public function logout()
    {
        throw new \Exception('Internal server error!');
    }
}