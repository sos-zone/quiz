<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Question;
use App\Service\QuizRequestProcessor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class QuizController extends AbstractController
{
    /**
     * @param Request $request
     * @param QuizRequestProcessor $quizRequestProcessor
     *
     * @return Response
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function index(
        Request $request,
        QuizRequestProcessor $quizRequestProcessor
    ) {
        // uncomment if only 1 survey per user is allowed
//        if ($this->getUser()->hasQuizzes()) {
//            return $this->redirectToRoute('result_page');
//        }

        if ($isCompleted = $quizRequestProcessor->process($request)) {
            return $this->redirectToRoute('result_page');
        }

        return $this->render('quiz/index.html.twig', [
            'form' => $quizRequestProcessor->getForm()->createView(),
        ]);
    }

    /**
     * @return Response
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function result()
    {
        if (!$this->getUser()->hasQuizzes()) {
            return $this->redirectToRoute('home_page');
        }

        $questionRepository = $this->getDoctrine()->getRepository(Question::class);

        return $this->render('quiz/result.html.twig', [
            'questionStatistics' => $questionRepository->findQuestionStatistics(),
        ]);
    }
}