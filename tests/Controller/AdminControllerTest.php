<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\DataFixtures\UserFixtures;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class AdminControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $client->request('GET', '/admin');
        $this->assertEquals(Response::HTTP_TEMPORARY_REDIRECT, $client->getResponse()->getStatusCode());

        $client = $this->createAuthorizedClient(UserFixtures::USER_EMAIL);
        $client->request('GET', '/admin');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());

        $client = $this->createAuthorizedClient(UserFixtures::ADMIN_EMAIL);
        $client->request('GET', '/admin');
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    protected function createAuthorizedClient(string $userEmail)
    {
        $client = static::createClient();
        $container = static::$kernel->getContainer();
        $session = $container->get('session');
        $person = self::$kernel->getContainer()->get('doctrine')->getRepository(User::class)
            ->findOneByEmail($userEmail);

        $token = new UsernamePasswordToken($person, null, 'main', $person->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();

        $client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));

        return $client;
    }
}