<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\Question;
use App\Service\QuestionPaginator;
use App\Repository\QuestionRepository;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Knp\Component\Pager\Paginator;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

class QuestionPaginatorTest extends TestCase
{
    /**
     * @var QuestionPaginator
     */
    protected $testClass;

    public function setUp()
    {
        $this->testClass = new QuestionPaginator(
            $this->getPaginatorMock(),
            $this->getEntityManagerMock()
        );
    }

    public function testIsLastPage()
    {
        $this->assertEquals(false, $this->testClass->isLastPage(1));

        $this->assertEquals(true, $this->testClass->isLastPage(2));

        $this->expectException(InvalidArgumentException::class);
        $this->testClass->isLastPage(3);
    }

    private function getPaginatorMock()
    {
        return $this->createMock(Paginator::class);
    }

    private function getEntityManagerMock()
    {
        $questionMock = $this->createMock(Question::class);

        $questionRepositoryMock = $this->createMock(QuestionRepository::class);
        $questionRepositoryMock->method('findQuizQuestions')
            ->willReturn([
                $questionMock,
                $questionMock,
                $questionMock,
                $questionMock,
                $questionMock,
                $questionMock,
                $questionMock,
                $questionMock,
                $questionMock,
                $questionMock,
        ]);

        $emMock = $this->createMock(EntityManager::class);
        $emMock->method('getRepository')
            ->willReturn($questionRepositoryMock);

        return $emMock;
    }
}