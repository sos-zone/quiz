import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../node_modules/bootstrap-social/bootstrap-social.css';
import '../../node_modules/font-awesome/css/font-awesome.min.css';
import '../../node_modules/chart.js/dist/Chart.css';
import '../css/app.scss';

import '../../node_modules/jquery/dist/jquery.min.js';
import '../../node_modules/popper.js';
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../../node_modules/chart.js/dist/Chart.js';
